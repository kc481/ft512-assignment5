import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(){
		this.plays = null;
		this.customer = null;
		this.performances = null;
	}

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public Play play_for(Performance aperformance){
		return plays.get(aperformance.getPlayID());
	}

	public int amount_for(Performance aperformance){
		int result = 0;
		switch (play_for(aperformance).getType()) {
			case "tragedy": result = 40000;
				if (aperformance.getAudience() > 30) {
					result += 1000 * (aperformance.getAudience() - 30);
				}
				break;
			case "comedy":  result = 30000;
				if (aperformance.getAudience() > 20) {
					result += 10000 + 500 * (aperformance.getAudience() - 20);
				}
				result += 300 * aperformance.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  play_for(aperformance).getType());
		}
		return result;
	}

	int volumecredits_for(Performance aperformance) {
		int result = 0;
		result += Math.max(aperformance.getAudience() - 30, 0);
		if ( play_for(aperformance).getType().equals("comedy")) {
			result += Math.floor((double) aperformance.getAudience() / 5.0);
		}
		return result;
	}

	public int total_volumecredits(){
		int result = 0;
		for (Performance perf: performances) {
			result += volumecredits_for(perf);
		}
		return result;
	}

	public int total_amount() {
		int result = 0;
		for (Performance perf: performances) {
			result += amount_for(perf);
		}
		return result;
	}

	public String statement() {
		BillPrint statementdata = new BillPrint();
		statementdata.customer = this.customer;
		statementdata.performances = this.performances;
		return render_plaintext(statementdata);
	}

	public String render_plaintext(BillPrint data){
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		String result = "Statement for " + data.customer + "\n";
		for (Performance perf: data.performances) {
			if (play_for(perf) == null) {
				throw new IllegalArgumentException("No play found");
			}

			// print line for this order
			result += "  " + play_for(perf).getName() + ": $" + numberFormat.format((double) amount_for(perf) / 100.00) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}
		int totalAmount = total_amount();
		int volumeCredits = total_volumecredits();
		result += "Amount owed is $" + numberFormat.format((double) totalAmount / 100) + "\n";
		result += "You earned " + volumeCredits + " credits" + "\n";
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
